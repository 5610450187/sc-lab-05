package Control;

import java.util.ArrayList;

import Model.Book;
import Model.Library;
import Model.People;
import Model.ReferencesBook;

public class LibrarySystem {
	Library library;
	People people1;
	People people2;
	ArrayList<Book> listBook;
	ArrayList<ReferencesBook> listRef;
	
	public LibrarySystem() {
		library = new Library();
		listBook = new ArrayList<Book>();
		listRef = new ArrayList<ReferencesBook>();
		people1 = new People("Ariyamak", "5610450381", "D14");
		people2 = new People("Thanatchaporn", "5610450187", "D14");
		
		library.addBook(new Book("Big Java",100,"Cay S. Horstmann",2013));
		library.addBook(new Book("DATABASE SYSTEM", 101,"Jeffrey D. Ullman", 2010));
		library.addBook(new Book("PPL", 102,"Devenbra KrSharma", 2014));
		library.addBook(new Book("Let Us C++", 103,"Yashavant Kanetkan", 2011));
		
		listBook = library.getAllBook();
		System.out.println("- ALL BOOK -");
		printAllBook(listBook);
		
		System.out.println("-------------------------------------------------------------");
		
		library.addReference(new ReferencesBook("A DAY", 200, "STAMP", 2014));
		library.addReference(new ReferencesBook("CHANNEL", 201, "CoCo Channel", 2011));
	
		library.addReference(new ReferencesBook("CNN Newspaper", 300, "Time Square", 2014));
		library.addReference(new ReferencesBook("PPC Newspaper", 301, "Post Today", 2014));
		
		listRef = library.getAllReference();
		System.out.println("- ALL REFERENCE -");
		printAllRef(listRef);
		
		System.out.println("\n" + library.searchBook("Big Java"));
		System.out.println(library.searchBook(102));
		System.out.println(library.searchBook("nkjnckjendk"));
		
		System.out.println(library.searchReference("CNN Newspaper"));
		System.out.println(library.searchReference(201));
		
		
		System.out.println("-------------------------------------------------------------");
		
		System.out.println(library.borrowBook(people1, "PPL"));
		System.out.println(library.borrowBook(people2,"Big Java"));	
		System.out.println(library.borrowBook(people1,"Java"));
		System.out.println(library.borrowBook(people2,101));
		System.out.println(library.borrowBook(people2,201));
		
		System.out.println("-------------------------------------------------------------");
		
		System.out.println(library.borrowReference("CNN Newspaper"));
		
		System.out.println("-------------------------------------------------------------");
		
		System.out.println(library.receiveBook(people1,"MATH"));
		System.out.println(library.receiveBook(people1,"PPL"));
		


	}

	public static void main(String[] args) {
		new LibrarySystem();
	}
	
	public void printAllBook(ArrayList<Book> listOfBook){
		for (Book book : listOfBook) {
			String result = (String)((book.getName() +" "+ book.getId() +" "+book.getAuthor()+ " "+book.getYears()));
			System.out.println(result);
			
		}
	}
	
	public void printAllRef(ArrayList<ReferencesBook> listOfRef){
		for (ReferencesBook referencesBook : listOfRef) {
			String result = (String)((referencesBook.getName() +" "+ referencesBook.getId() +" "+referencesBook.getAuthor()+ " "+referencesBook.getYears()));
			System.out.println(result);
			
		}
		
	}
	
	
	
	
}