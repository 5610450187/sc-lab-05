package Model;

import java.util.ArrayList;

import javax.rmi.CORBA.Stub;

public class Library {

	Book bookClass;
	ReferencesBook ref;

	ArrayList<Book> libraryBook;
	ArrayList<ReferencesBook> libraryReferencesBooks;

	String reference;


	public Library(){
		libraryBook = new ArrayList<Book>();
		libraryReferencesBooks = new ArrayList<ReferencesBook>();
	}

	public String searchBook(String name){
		String output = "";
		for (Book book : libraryBook) {
			if(book.getName() == name){
				return book.getName()+" is Available";
			}
		}
		
		return "Can not find this book.";
	}

	public String searchBook(int id){
		for (Book book : libraryBook) {
			if(book.getId() == id){
				return book.getName()+" is Available";
			}
		}
		return "Can not find this book.";
		
	}

	public String borrowBook(People people,int id){
		String output = "";
		boolean chkBorrow = false;
		for (Book book : libraryBook) {
			if(book.getId() == id){
				if(book.getStatus() == "Available"){
					output = "You can borrow "+book.getName();
					chkBorrow = true;
					people.addBookToInventory(book);
					break;
				}
				else {
					output = "You can not borrow "+book.getName();
					chkBorrow = true;
					break;
				}
			}
		}
		if(!chkBorrow){
			output = "Don't have "+ id;
		}
		return output;
		
		}
	

	public String borrowBook(People people,String name){
		String output = "";
		boolean chkBorrow = false;
		for (Book book : libraryBook) {
			if(book.getName() == name){
				if(book.getStatus() == "Available"){
					output = "You can borrow "+book.getName();
					chkBorrow = true;
					people.addBookToInventory(book);
					break;
				}
				else {
					output = "You can not borrow "+book.getName();
					chkBorrow = true;
					break;
				}
			}
		}
		if(!chkBorrow){
			output = "Don't have "+name;
		}
		return output;
	}

	public String receiveBook(People people,String name){
		String output = "";
		for (Book book : people.getAllBook()) {
			if (book.getName() == name){
				book.setStatus("Available");
				output = "Thank you";
				people.removeBookFromInventory(book);
				break;
			}
			else{
				output = "Don't Found";
			}
		}
		return output;
	}
	
	public String receiveBook(People people,int id){
		String output = "";
		for (Book book : people.getAllBook()) {
			if (book.getId() == id){
				book.setStatus("Available");
				output = "Thank you";
				people.removeBookFromInventory(book);
				break;
			}
			else{
				output = "Don't Found";
			}
			}
		return output;
			
	}

	public void addBook(Book book){
		libraryBook.add(book);
	}

	public void removeBook(String bookName) {
		for (Book book : libraryBook) {
			if (book.getName() == bookName){
				libraryBook.remove(book);
			}
		}
		
	}
	
	public void removeBook(int bookId) {
		for (Book book : libraryBook) {
			if (book.getId() == bookId){
				libraryBook.remove(book);
			}
		}
		
	}

	public ArrayList<Book> getAllBook() {
		return libraryBook;
	}



	public String searchReference(String name){
		for (ReferencesBook ref : libraryReferencesBooks) {
			if(ref.getName() == name){
				return ref.getName()+" is Available";
			}
		}
		ref.setStatus("Available");
		return reference;
	}

	public String searchReference(int id){
		for (ReferencesBook ref : libraryReferencesBooks) {
			if(ref.getId() == id){
				return ref.getName()+" is Available";
			}
		}
		ref.setStatus("Available");
		return reference;
	}

	public boolean borrowReference(int id){
		return false;
	}

	public boolean borrowReference(String name){
		return false;
	}

	public void addReference(ReferencesBook ref){
		libraryReferencesBooks.add(ref);
	}


	public void removeReference() {
		libraryReferencesBooks.remove(ref);
	}

	public ArrayList<ReferencesBook> getAllReference() {
		return libraryReferencesBooks;
	}

}
