package Model;

public class ReferencesBook {
	private String name;
	private int id;
	private String author;
	private int years;
	private String status;
	
	public ReferencesBook(String name, int id, String author, int years) {
		this.name = name;
		this.id = id;
		this.author = author;
		this.years = years;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}
	
	public String setStatus(String status){
		return status;
	}
	

}
