package Model;

public class Book {
	private String name;
	private int id;
	private String author;
	private int years;
	private String status;
	
	
	public Book(String name, int id, String author, int years) {
		this.name = name;
		this.id = id;
		this.author = author;
		this.years = years;
		this.status = "Available";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAuthor() {
		this.author = author;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}
	
	
	public void setStatus(String status){
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	

}
