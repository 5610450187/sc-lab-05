package Model;

import java.util.ArrayList;

public class People {
	private String name;
	private String id;
	private String department;
	private ArrayList<Book> inventoryBook;
	
	
	
	public People(String name,String id,String department) {
		this.name = name;
		this.id = id;
		this.department = department;
		inventoryBook = new ArrayList<Book>();
	}
	
	public void Behaviour (String habbit){
		ArrayList<String> list = new ArrayList<String>();
		list.add(habbit);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	public void addBookToInventory(Book book) {
		inventoryBook.add(book);
	}
	
	public void removeBookFromInventory(Book book) {
		inventoryBook.remove(book);
	}
	
	public String getBookFromInventory() {
		String allBook = "";
		int count = 1;
		for (Book book : inventoryBook) {
			allBook+=count+". "+book.getName()+"\n";
			count++;
		}
		return allBook;
	}
	
	public ArrayList<Book> getAllBook(){
		return inventoryBook;
	}
	
	
}
